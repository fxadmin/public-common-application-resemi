FROM golang:alpine AS builder

WORKDIR /go/src/resemi/

COPY main.go ./

RUN go get -d -v github.com/mediocregopher/radix.v2/redis && \
    go get -d -v github.com/golang/glog && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o resemi .

FROM scratch

COPY --from=builder /go/src/resemi/resemi /

USER 65534:65534

ENTRYPOINT ["/resemi"]
